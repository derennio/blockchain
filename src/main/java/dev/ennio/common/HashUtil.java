package dev.ennio.common;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashUtil {
    /** The algorithm to use for hashing. */
    public static String sha256(String input) {
        return sha256(input, 1);
    }

    /** The algorithm to use for hashing. */
    public static String sha256(String input, int iterations) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            for (int i = 0; i < iterations; i++) {
                hash = digest.digest(hash);
            }

            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
