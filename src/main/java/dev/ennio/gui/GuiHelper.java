package dev.ennio.gui;

import dev.ennio.data.Block;
import dev.ennio.simulation.LocalPeer;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ennio
 **/
public class GuiHelper extends Component
{
    /** Window properties. **/
    private final int Width;
    private final int Height;

    /**
     * Java Swing fields that need to be updated
     * or accessed during from another scope runtime.
     **/
    private JLabel StatusLabel;

    /**
     * The local peers that will be used to communicate with the network (simulated).
     **/
    private final List<LocalPeer> RegisteredPeers;

    public GuiHelper()
    {
        this.RegisteredPeers = new ArrayList<>();

        this.Width = 400;
        this.Height = 280;

        LocalPeer daemon = new LocalPeer("Daemon", null);

        this.RegisteredPeers.add(daemon);
        this.RegisteredPeers.add(new LocalPeer("Peer 1", daemon));
    }

    /**
     * Method for setting up the main frame and handling
     * any kind of user interaction.
     */
    public void setupFrame()
    {
        JFrame mainFrame = new JFrame("BlockChain Demo");

        JLabel infoLabel = new JLabel("Derzeit sind  " + RegisteredPeers.size() + " Peers vorhanden");
        infoLabel.setBounds(100, 10, 250, 30);

        this.StatusLabel = new JLabel("");
        this.StatusLabel.setBounds(100, 35, 200, 30);

        JButton confirm = new JButton("Simulieren");
        confirm.setBounds(100, 100, 170, 30);
        confirm.addActionListener(event ->
        {
            try
            {
                runSimulation();
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        });

        JButton validate = new JButton("Validieren");
        validate.setBounds(100, 150, 170, 30);
        validate.addActionListener(event -> validateChains());

        JButton export = new JButton("Exportieren");
        export.setBounds(100, 200, 170, 30);
        export.addActionListener(event -> saveHashes());

        mainFrame.add(this.StatusLabel);
        mainFrame.add(confirm);
        mainFrame.add(validate);
        mainFrame.add(infoLabel);
        mainFrame.add(export);

        mainFrame.setSize(this.Width, this.Height);
        mainFrame.setLayout(null);
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Method for simulating blockchain workflow.
     **/
    private void runSimulation() throws InterruptedException
    {
        for (LocalPeer peer : this.RegisteredPeers) {
            for (int i = 0; i < 3; i++) {
                Block block = new Block(
                        peer.getBlockChain().getBlockAmount(),
                        peer.getBlockChain().getBackBlockHash(),
                        List.of("Test-Text " + i + " für Block " + peer.getBlockChain().getBlockAmount()));

                try {
                    peer.mine(block);
                    this.StatusLabel.setText(String.format("%s mined: %s", peer.Name, block.getHash()));
                } catch (IllegalArgumentException ex) {
                    this.StatusLabel.setText(String.format("%s invalid: %s", peer.Name, block.getHash()));
                }
            }

            peer.getBlockChain().getBlocks().forEach(block -> {
                System.out.println(block.getHash());
            });
        }
        int total = 0;
        for (LocalPeer peer : this.RegisteredPeers) {
            total += peer.getBlockChain().getBlockAmount();
        }

        this.StatusLabel.setText("Erfolgreich gemined: " + total);
    }

    /**
     * Method for validating the current chain.
     **/
    private void validateChains() {
        boolean result = true;

        for (LocalPeer peer : this.RegisteredPeers) {
            if (!peer.getBlockChain().validate()) {
                result = false;
            }
        }

        if (result) {
            this.StatusLabel.setText("Alle Blockchains sind gültig.");
        } else {
            this.StatusLabel.setText("Min. eine Blockchain ist korrupt.");
        }
    }

    /**
     * Method for saving the current chain to a file.
     */
    private void saveHashes() {
        File file = new File("hashes.txt");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            for (LocalPeer peer : this.RegisteredPeers) {
                for (Block block : peer.getBlockChain().getBlocks()) {
                    writer.write(block.getHash() + "\n");
                }
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
