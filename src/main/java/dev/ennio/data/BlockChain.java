package dev.ennio.data;

import java.util.ArrayList;
import java.util.List;

public class BlockChain implements IBlockChain {
    private final List<Block> blocks;

    public BlockChain(int genesisIndex) {
        this.blocks = new ArrayList<>();

        if (genesisIndex == 0) {
            List<String> genesisData = List.of("Genesis block");
            this.blocks.add(new Block(0, "00000000000000", genesisData));
        }

        System.out.println("Blockchain initialized");
    }

    /**
     * @return the number of blocks in the chain
     */
    @Override
    public int getBlockAmount() {
        return this.blocks.size();
    }

    /**
     * Adds a block to the chain
     * @param block the block to be added to the chain
     * @return true if the block was added, false otherwise
     */
    @Override
    public boolean addBlock(Block block) {
        if (block.getPreviousHash().equals(this.getBackBlockHash())) {
            this.blocks.add(block);
            return true;
        }

        return false;
    }

    /**
     * @return the hash of the last block in the chain
     */
    @Override
    public String getBackBlockHash() {
        return this.blocks.get(this.blocks.size() - 1).getHash();
    }

    /**
     * Validates the chain.
     * @return true if the chain is valid, false otherwise
     */
    @Override
    public boolean validate()
    {
        List<Block> subChain = this.blocks.subList(1, this.blocks.size());
        Block previousBlock = this.blocks.get(0);

        for (Block currentBlock : subChain) {
            if (!currentBlock.getPreviousHash().equals(previousBlock.getHash())) {
                return false;
            }
            previousBlock = currentBlock;
        }

        return true;
    }

    /**
     * @return the chain
     */
    @Override
    public List<Block> getBlocks()
    {
        return this.blocks;
    }
}
