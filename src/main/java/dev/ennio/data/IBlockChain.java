package dev.ennio.data;

import java.util.List;

public interface IBlockChain
{
    int getBlockAmount();
    boolean addBlock(Block block);
    String getBackBlockHash();
    boolean validate();
    List<Block> getBlocks();
}
