package dev.ennio.data;

import dev.ennio.common.HashUtil;

import java.util.List;

/**
 * Block class that holds all required data for a block.
 */
public class Block implements Cloneable {
    private final int index;
    private final String hash;
    private final String previousHash;
    private final long timestamp;
    private final List<String> data;

    public Block(
            int index,
            String previousHash,
            List<String> data) {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = System.currentTimeMillis();
        this.data = data;

        this.hash = HashUtil.sha256(this.previousHash + this.timestamp + this.data);
    }

    /** Getters and setters */
    public int getIndex() {
        return index;
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public List<String> getData() {
        return data;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}