package dev.ennio.simulation;

import dev.ennio.data.Block;
import dev.ennio.data.BlockChain;
import dev.ennio.data.IBlockChain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ennio
 **/
public class LocalPeer
{
    public String Name;

    private IBlockChain BlockChain;
    private List<LocalPeer> Peers;

    public LocalPeer(String name, LocalPeer connectedPeer)
    {
        this.Name = name;
        this.BlockChain = new BlockChain(0);

        if (connectedPeer != null) {
            registerToNetwork(connectedPeer);
            getBlockChain(connectedPeer);
        } else {
            Peers = new ArrayList<>();
        }
    }

    /**
     * Adds a block to the local blockchain
     * @param block the block to be added
     */
    public void add(Block block)
    {
        try {
            this.BlockChain.addBlock(block);
        } catch (IllegalArgumentException e) {
            if (this.Peers.size() > 0) {
                getBlockChain(this.Peers.get(0));
            }
        }
    }

    /**
     * Mines a new block
     * @param block the block to be mined
     */
    public void mine(Block block)
    {
        this.BlockChain.addBlock(block);
        this.Peers.forEach((node) -> {
            try {
                node.add((Block) block.clone());
            } catch (CloneNotSupportedException ex) {
                ex.printStackTrace();
            }
        });
    }

    /**
     * Registers a peer to the network
     * @param connectedPeer the peer to be registered to
     */
    private void registerToNetwork(LocalPeer connectedPeer)
    {
        this.Peers = connectedPeer.getPeers();
        this.Peers.add(connectedPeer);
    }

    /**
     * Gets the blockchain of a peer and overrides the local blockchain
     * @param connectedPeer the peer to get the blockchain from
     */
    private void getBlockChain(LocalPeer connectedPeer)
    {
        this.BlockChain = connectedPeer.BlockChain;
    }

    /**
     * @return the peers of the local peer
     */
    public List<LocalPeer> getPeers()
    {
        return this.Peers;
    }

    /**
     * @return the local blockchain
     */
    public IBlockChain getBlockChain()
    {
        return BlockChain;
    }
}
